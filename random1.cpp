#include<iostream>
#include<random>
#include<cstdlib>
#include<string>

using namespace std;

int main(){ 
   int i;

   static random_device rd;
   static mt19937_64 RNG( rd() );
   static bernoulli_distribution boolRNG( 0.5 );

   i = rd()%10;
   cout << i << endl;
   
   //cout << boolRNG(RNG) << endl;
   cout << boolalpha <<  boolRNG(RNG) << endl;
}
