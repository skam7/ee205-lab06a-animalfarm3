///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   02_18_2021
///////////////////////////////////////////////////////////////////////////////

#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {
	
Nunu::Nunu( bool native, enum Color newColor, enum Gender newGender ) {
	gender = newGender;         	
   species = "Fistularia chinensis";    	
   scaleColor = newColor;	
   favoriteTemp = 80.6;  
   isNative = native;
}

//const string Nunu::speak() {
//   return string( "Bubble bubble" );
//}


void Nunu::printInfo() {
	cout << "Nunu" << endl;
	cout << "   Is native = [" << boolalpha << isNative << "]" << endl;
   Fish::printInfo();
}

} // namespace animalfarm
