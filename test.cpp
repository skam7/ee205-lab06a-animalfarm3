///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file test.cpp
/// @version 1.0
///
/// Exports data about all palila
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   02_18_2021
///////////////////////////////////////////////////////////////////////////////
#include<iostream>
#include <array>
#include <list>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "fish.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "bird.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main(){  
   std::list<Animal*> animalList;
   for(int i = 0; i < 25; i++){
      animalList.push_front(AnimalFactory::getRandomAnimal());
   }
   cout << endl;

   for(Animal* animal : animalList){
      cout << "Name: " << animal->Animal::getRandomName() << endl;
      cout << "Gender: " << animal->Animal::genderName(Animal::getRandomGender()) << endl;
      cout << "Weight: " << animal->getRandomWeight(5, 21) << endl;
      cout << "Color: " << animal->Animal::colorName(Animal::getRandomColor()) << endl;
      cout << "Bool: " << boolalpha << animal->getRandomBool() << endl;
      cout << "Speak: " << animal->speak() << endl;
   }
   

   return 0;
}
