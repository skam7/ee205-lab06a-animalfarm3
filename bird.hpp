///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file bird.hpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   02_18_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include "animal.hpp"

namespace animalfarm {

class Bird : public Animal {
public:
	enum Color featherColor;
	bool        isMigratory;

   virtual const string speak();

	void printInfo();
};

} // namespace animalfarm
