///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Shannon Kam <skam7@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   03_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"

using namespace std;
using namespace animalfarm;

int main() {
   cout << "Welcome to Animal Farm 3" << endl;
   
   std::array<Animal*, 30> animalArray;
   animalArray.fill( NULL );
   for(int i = 0; i < 25; i++){
      animalArray[i] = AnimalFactory::getRandomAnimal();
   }
   cout << endl;
   cout << "Array of Animals: " << endl;
   cout << "   Is it empty: " << boolalpha << animalArray.empty() << endl;
   cout << "   Number of Elements: " << animalArray.size() << endl;
   cout << "   Max size: " << animalArray.max_size() << endl;
   
   for(Animal* animal : animalArray){
      if(animal == NULL){
         break;
      }
      cout << animal->speak() << endl; 
   }
   
   //destructors
   for( int i = 0; i < 25; i++){
      delete animalArray[i];
   }

   std::list<Animal*> animalList;
   for(int i = 0; i < 25; i++){
      animalList.push_front(AnimalFactory::getRandomAnimal());
   }
   cout << endl;
   cout << "List of Animals: " << endl;
   cout << "   Is it empty: " << boolalpha << animalList.empty() << endl;
   cout << "   Number of Elements: " << animalList.size() << endl;
   cout << "   Max size: " << animalList.max_size() << endl;
   
   for(Animal* animal : animalList){
      cout << animal->speak() << endl;
   }
    
   while(!animalList.empty()){
      delete animalList.front();
      animalList.pop_front();
   }
   return 0;
}
